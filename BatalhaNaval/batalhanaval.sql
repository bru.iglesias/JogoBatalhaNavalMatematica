-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 10/06/2018 às 22:00
-- Versão do servidor: 10.1.26-MariaDB-0+deb9u1
-- Versão do PHP: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `batalhanaval`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `pergunta`
--

CREATE TABLE `pergunta` (
  `id` int(11) NOT NULL,
  `questao` varchar(100) NOT NULL,
  `opcao1` int(11) NOT NULL,
  `opcao2` int(11) NOT NULL,
  `opcao3` int(11) NOT NULL,
  `opcao4` int(11) NOT NULL,
  `correta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Fazendo dump de dados para tabela `pergunta`
--

INSERT INTO `pergunta` (`id`, `questao`, `opcao1`, `opcao2`, `opcao3`, `opcao4`, `correta`) VALUES
(1, 'O resultado da expressao: 2 * 4 + 2 = ?', 10, 12, 13, 15, 10),
(2, 'O resultado da expressao: 2 * 4 - 2 = ?', 2, 4, 6, 5, 6),
(3, 'O resultado da expressao: 8 / 4 - 1 = ?', 2, 4, 3, 1, 1),
(4, 'O resultado da expressao:  8 + 4 / 2 = ?', 6, 12, 10, 11, 10),
(5, 'O resultado da expressao:  7 - 4 * 1 = ?', 1, 2, 3, 4, 3),
(6, 'O resultado da expressao:  8 / 4 * 2 = ?', 4, 3, 2, 1, 4),
(7, 'O resultado da expressao:  9 * 1 - 1 = ?', 0, 1, 8, 9, 8),
(8, 'O resultado da expressao:  4 / 4 * 5 = ?', 3, 5, 4, 6, 5),
(9, 'O resultado da expressao:  9 / 3 + 4 = ?', 5, 6, 7, 8, 7),
(10, 'O resultado da expressao:  5 - 3 * 4 = ?', 5, 6, 7, 8, 8),
(11, 'O resultado da expressao: 4 * 5 - 3 * 4 = ?', 9, -12, 32, 8, 8),
(12, 'O resultado da expressao: 9 / 3 - 1 * 2 = ?', 2, 3, 4, 6, 2),
(13, 'O resultado da expressao: (9 + 3) / 1 * 2 = ?', 2, 3, 5, 6, 6),
(14, 'O resultado da expressao: 9 + (3 / 1) * 2 = ?', 24, 23, 15, 16, 15),
(15, 'O resultado da expressao: 8 + (2 / 1) * 3 = ?', 24, 23, 15, 14, 14),
(16, 'O resultado da expressao: 16 / 4 - 2 = ?', 8, 2, 3, 4, 2),
(17, 'O resultado da expressao: 7 - 3 * 5 + 10 = ?', 2, 3, 30, 29, 2),
(18, 'O resultado da expressao: 5 - 2 * 5 + 10 = ?', 5, 4, 25, -25, 5),
(19, 'O resultado da expressao: 5 + 2 * 5 - 10 = ?', 5, 4, 2, -2, 5),
(20, 'O resultado da expressao: 3 + 2 * 7 - 10 = ?', 35, 25, 6, 7, 7),
(21, 'O resultado da expressao:  20 - 3 * 4 = ?', 5, 8, 7, 6, 8),
(22, 'O resultado da expressao:  30 - 2 * 10 = ?', 210, 200, 9, 10, 10),
(23, 'O resultado da expressao:  30 - 2 * 10 / 2 = ?', 140, 120, 20, 9, 20),
(24, 'O resultado da expressao: 2 * 10 / 5 - 1 = ?', 3, 2, 20, 21, 3),
(25, 'O resultado da expressao: 5 * 10 / 2 - 1 = ?', 23, 24, 2, 21, 24),
(26, 'O resultado da expressao: 5 * 10 - 4 / 2 = ?', 44, 46, 48, 42, 48),
(27, 'O resultado da expressao: 7 * 2 - 4 / 2 = ?', 10, 12, 14, 16, 12),
(28, 'O resultado da expressao: 7 * 3 - 9 / 3 = ?', 18, 16, 10, 20, 18),
(29, 'O resultado da expressao: 7 * 3 - 1 / 1 = ?', 18, 20, 10, 12, 20),
(30, 'O resultado da expressao: 1 * 5 - 2 / 2 = ?', 4, 3, 10, 12, 4);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `pergunta`
--
ALTER TABLE `pergunta`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
