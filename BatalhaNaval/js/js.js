//############### Variaveis para o AJAX ########################
// variáveis para os objetos XMLHttpRequest
var ajaxPergunta;
var ajaxOpcao1;
var ajaxOpcao2;
var ajaxOpcao3;
var ajaxOpcao4;
var ajaxResposta;
// variaveis para gerar o indice aleatório para consulta no banco de dados 
var n;
var numero;
// Variaveis para guardar os valores de retorno
var questao;
var opcao1;
var opcao2;
var opcao3;
var opcao4;
var resposta;
//############### Fim Variaveis para o AJAX ########################
// nivel inicial do jogo
var nivel = 1;
var pontos=0;
var respondidas = 0;
var certas = 0;
var erradas = 0;

// Função para reiniciar o jogo
function reiniciaJogo() {
	somGameover.pause();
	$("#fim").remove();
	nivel = 1;
	pontos=0;
	respondidas = 0;
	certas = 0;
	erradas = 0;
	start();	
} //Fim da função reiniciaJogo

// Função para avancar no jogo
function avancarJogo() {
	somGameover.pause();
	$("#fim").remove();
	start();
} //Fim da função reiniciaJogo

// Função que cria os objetos ajax XMLHttpRequest()
function openAjax()
{
ajaxPergunta = new XMLHttpRequest();
ajaxOpcao1= new XMLHttpRequest();
ajaxOpcao2= new XMLHttpRequest();
ajaxOpcao3= new XMLHttpRequest();
ajaxOpcao4= new XMLHttpRequest();
ajaxResposta= new XMLHttpRequest();
}// Fim da Função que cria os objetos ajax XMLHttpRequest()


// Função que inicia o jogo
function start()
{             
	$("#inicio").hide();
	// Adiciona elementos visuais
	$("#fundoGame").append("<div id='tabuleiro'></div>");
	$("#fundoGame").append("<div id='placar'></div>");
	$("#fundoGame").append("<div id='vida'></div>");
	$("#fundoGame").append("<div id='vidainimigo'></div>");
	
	// Adiciona os personagens
	$("#tabuleiro").append("<div id='jogador' class='animacaoheroi'></div>");
	$("#tabuleiro").append("<div id='inimigo' class='animacaoinimigo'></div>");
	
	// variaveis do jogo 
	var jogo = {}
	var TECLA= {W:87, S:83, D:68, A:65, J:74, K:75, }

	// Essa flag determina se o inimigo se o inimigo vai mover para cima ou para baixo
	flag = true;

	// Determina se os tiros do jogador causam dano a vida do inimigo
	tirarVida = true;

	var velocidadeinimigo = 5;
	var velocidadejogador = 5;
	var podeAtirar = true;
	var vidaAtual=3;
	var vida_inimigo = 100;
	var mostrarPergunta = true;
	var exibirInstrucao = true;


	// bandeira para verificar se a questao já foi respondida
	var countQuestao = 1;

	// variáveis para elementos de áudio
	var somDisparo=document.getElementById("somDisparo");
	var somExplosao=document.getElementById("somExplosao");
	var musica=document.getElementById("musica");
	var somGameover=document.getElementById("somGameover");
	var somPerdido=document.getElementById("somPerdido");
	var somAcerto=document.getElementById("somAcerto");

	// inicia a musica de fundo do jogo
	musica.addEventListener("ended", function(){ musica.currentTime = 0; musica.play(); }, false);
	musica.play();

	
	// Função loop do jogo
	jogo.timer = setInterval(loop,30);
	function loop() 
	{
		
		movejogador();
		movefundo(1); 
		moveinimigo();
		placar();
		colisao();	
		vida();
		vidainimigo();
		pergunta();
		instrucao();
		
	}
	//####################### FUNÇÕES AJAX	########################################
	// Gera a questao e adiciona no local certo no jogo (div)
	function geraPergunta()
	{        
       
	
		function atualizarPergunta()
		{
		
			if (ajaxPergunta.readyState == 4)
			{
			questao =  ajaxPergunta.responseText;
			console.log(questao);
			$("#tabuleiro").append("<div id='pergunta'>"+questao+"</div>");
			}
		}

        function buscarPergunta()
		{
            n = numero.toString();
			url = "php/geradorQuestao.php?A=" + n;
			ajaxPergunta.open("GET",url,true);
			ajaxPergunta.onreadystatechange = atualizarPergunta;
			ajaxPergunta.send();
        }
		buscarPergunta();
		console.log("pergunta ok");
	}
	// Gera a opção 1 de resposta e adiciona no local certo no jogo (div)
	function geraOpcao1()
	{        
       
	
		function atualizarOpcao1()
		{
		
			if (ajaxOpcao1.readyState == 4)
			{
			opcao1 =  ajaxOpcao1.responseText;
			console.log(opcao1);
			$("#tabuleiro").append("<div id='resposta1'><br> <b>"+opcao1+" </b> </div>");
			}
		}

        function buscarOpcao1()
		{
            n = numero.toString();
			url = "php/geradorOpcao1.php?A=" + n;
			ajaxOpcao1.open("GET",url,true);
			ajaxOpcao1.onreadystatechange = atualizarOpcao1;
			ajaxOpcao1.send();
        }
        buscarOpcao1();
	}
	// Gera a opção 2 de resposta e adiciona no local certo no jogo (div)
	function geraOpcao2()
	{        
		function atualizarOpcao2()
		{
		
			if (ajaxOpcao2.readyState == 4)
			{
			opcao2 =  ajaxOpcao2.responseText;
			console.log(opcao2);
			$("#tabuleiro").append("<div id='resposta2'><br> <b>"+opcao2+" </b> </div>");
			}
		}

        function buscarOpcao2()
		{
            n = numero.toString();
			url = "php/geradorOpcao2.php?A=" + n;
			ajaxOpcao2.open("GET",url,true);
			ajaxOpcao2.onreadystatechange = atualizarOpcao2;
			ajaxOpcao2.send();
        }
        buscarOpcao2();
	}
	// Gera a opção 3 de resposta e adiciona no local certo no jogo (div)	
	function geraOpcao3()
	{        
		function atualizarOpcao3()
		{
		
			if (ajaxOpcao3.readyState == 4)
			{
			opcao3 =  ajaxOpcao3.responseText;
			console.log(opcao3);
			$("#tabuleiro").append("<div id='resposta3'><br> <b>"+opcao3+" </b> </div>");
			}
		}

        function buscarOpcao3()
		{
            n = numero.toString();
			url = "php/geradorOpcao3.php?A=" + n;
			ajaxOpcao3.open("GET",url,true);
			ajaxOpcao3.onreadystatechange = atualizarOpcao3;
			ajaxOpcao3.send();
        }
        buscarOpcao3();
	}
	// Gera a opção 4 de resposta e adiciona no local certo no jogo (div)
	function geraOpcao4()
	{        
		function atualizarOpcao4()
		{
		
			if (ajaxOpcao4.readyState == 4)
			{
			opcao4 =  ajaxOpcao4.responseText;
			console.log(opcao4);
			$("#tabuleiro").append("<div id='resposta4'><br> <b>"+opcao4+" </b> </div>");
			}
		}

        function buscarOpcao4()
		{
            n = numero.toString();
			url = "php/geradorOpcao4.php?A=" + n;
			ajaxOpcao4.open("GET",url,true);
			ajaxOpcao4.onreadystatechange = atualizarOpcao4;
			ajaxOpcao4.send();
        }
        buscarOpcao4();
	}
	// Gera a resposta correta e guarda em variável 
	function geraResposta()
	{        
		function atualizarResposta()
		{
		
			if (ajaxResposta.readyState == 4)
			{
			resposta =  ajaxResposta.responseText;
			console.log(resposta);
			}
		}

        function buscarResposta()
		{
            n = numero.toString();
			url = "php/geradorResposta.php?A=" + n;
			ajaxResposta.open("GET",url,true);
			ajaxResposta.onreadystatechange = atualizarResposta;
			ajaxResposta.send();
        }
        buscarResposta();
    }
	//###########################	FIM AJAX	###############################


	//#################### Movimentos no Jogo ################################

	//Função move fundo (agua)
	function movefundo(vel)
	{
		esquerda = parseInt($("#fundoGame").css("background-position"));
		$("#fundoGame").css("background-position",esquerda-vel);
	}

	//Movimentos do Jogador
	//verifica se o usuario pressionou alguma tecla
	jogo.pressionou=[];
	$(document).keydown(function(e)
	{
		jogo.pressionou[e.which] = true;
	});

	$(document).keyup(function(e)
	{
		jogo.pressionou[e.which] = false;
	});
	
	// funçao para mover o jogador
	function movejogador()
	{
		if(jogo.pressionou[TECLA.W])
		{
			var topo = parseInt($("#jogador").css("top"));
			$("#jogador").css("top",topo - velocidadejogador);
			if(topo <= 100)
			{
				$("#jogador").css("top", topo + velocidadejogador);
			}
		}
		if(jogo.pressionou[TECLA.S])
		{
			var topo = parseInt($("#jogador").css("top"));
			$("#jogador").css("top",topo+velocidadejogador);

			if(topo >= 450)
			{
				$("#jogador").css("top",topo-velocidadejogador);
			}
		}
		if(jogo.pressionou[TECLA.D])
		{
			var lado = parseInt($("#jogador").css("left"));
			$("#jogador").css("left", lado +velocidadejogador);
			
			if(lado >= 373)
			{
				$("#jogador").css("left",lado);
			}
		}
		if(jogo.pressionou[TECLA.A])
		{
			var lado = parseInt($("#jogador").css("left"));
			$("#jogador").css("left", lado-velocidadejogador);

			if(lado <= 18)
			{
				$("#jogador").css("left",lado+velocidadejogador);
			}
		}

		if(jogo.pressionou[TECLA.J]){
			disparo();
		}

		if(jogo.pressionou[TECLA.K]){
			
		}

	} //fim da função move jogador


	//DISPAROS  DO JOGADOR
	
	function disparo()
	{
	 	if(podeAtirar == true)
	 	{
		 	somDisparo.play();
		 	podeAtirar = false;
		 	topo = parseInt($("#jogador").css("top"));
		 	posicaoX = parseInt($("#jogador").css("left"));
		 	tiroX = posicaoX + 150;
		 	topoTiro = topo + 50;
		 	$("#tabuleiro").append("<div id='disparo' class='disparo'></div>");
			$("#disparo").css("top",topoTiro);
			$("#disparo").css("left",tiroX);

			var tempoDisparo = window.setInterval(executarDisp,30);
		} 
	
		function executarDisp() 
		{
		 	posicaoX = parseInt($("#disparo").css("left"));
		 	$("#disparo").css("left",posicaoX+15);

		 		if(posicaoX > 900)
		 		{
		 			window.clearInterval(tempoDisparo);
		 			tempoDisparo = null;
					 $("#disparo").remove();
					 podeAtirar = true;
				 } 
				 
		 }//fim funcao executarDisp()

	}//fim funcao Disparo()


	//Função para mover o inimigo
	function moveinimigo()
	{
			if(flag)
			{
				movebaixo();
				if(posicaoY >= 450)
				{
					flag = false;
				}	
			}
			else
			{
				movecima();
				
				if(posicaoY <= 100)
				{
					flag = true;
				}
			}

		function movecima()
		{
			
			posicaoY = parseInt($("#inimigo").css("top"));
			$("#inimigo").css("top",posicaoY - velocidadeinimigo);
			
		} //fim da funcao movecima

		function movebaixo()
		{
			
			posicaoY = parseInt($("#inimigo").css("top"));
			$("#inimigo").css("top", posicaoY + velocidadeinimigo);
			
			
		}//fim da funcao movebaixo

	} //fim da funcao para mover o inimigo

	//################### FUNÇÕES DE COLISÕES ############################

	function colisao()
	{
		var colisao_disparoinimigo = ($("#disparo").collision($("#inimigo")));
		var jogador_resposta1 = ($("#jogador").collision($("#resposta1")));
		var jogador_resposta2 = ($("#jogador").collision($("#resposta2")));
		var jogador_resposta3 = ($("#jogador").collision($("#resposta3")));
		var jogador_resposta4= ($("#jogador").collision($("#resposta4")));

		// Disparo com o inimigo1	
		if (colisao_disparoinimigo.length>0) 
		{
				if(tirarVida == true)
				{
				pontos = pontos +100;
				vida_inimigo = vida_inimigo - 10;	
				}
				$("#disparo").css("left",950);
				explosaoinimigo();
			
		}

		// Jogador com resposta 1
		if (jogador_resposta1.length>0) 
		{
			if(opcao1 == resposta ) // o que acontece se a resposta estiver correta
			{
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				certas = certas+1;
				somAcerto.play();
				
			}
			else	// o que acontece se a resposta estiver errada
			{	
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				erradas = erradas +1;
				inimigoatira();
				
			}
		}

		// Jogador com resposta 2
		if (jogador_resposta2.length>0) 
		{
			if(opcao2 == resposta )
			{
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				certas = certas+1;
				somAcerto.play();
				
			}
			else
			{	
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				erradas = erradas +1;
				inimigoatira();
				
			}	
		}

		// Jogador com resposta 3
		if (jogador_resposta3.length>0) 
		{
			if(opcao3 == resposta )
			{
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				certas = certas+1;
				somAcerto.play();
				
			}
			else
			{	
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				erradas = erradas +1;
				inimigoatira();
			}	
			
		}

		// Jogador com resposta 4
		if (jogador_resposta4.length>0) 
		{
			if(opcao4 == resposta )
			{
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				certas = certas+1;
				somAcerto.play();
				
			}
			else
			{	
				$("#pergunta").remove();
				$("#resposta1").remove();
				$("#resposta2").remove();
				$("#resposta3").remove();
				$("#resposta4").remove();
				tirarVida = true;
				countQuestao = countQuestao + 1;
				mostrarPergunta = true;
				respondidas = respondidas +1;
				erradas = erradas +1;
				inimigoatira();
			}	
		}
	}//fim da funcao colisao


	//############################# perguntas #########################

	function pergunta()
	{
		if(vida_inimigo == 80 && mostrarPergunta && countQuestao == 1 )
		{
			$("#instrucao").remove();
			if (nivel ==1)
			{
				numero = Math.floor(Math.random() * 9) + 1; // 9 + 1 seleciona perguntas de indice 1 a 10 (nivel 1) e evita sorteio do 0
			}
			if (nivel ==2)
			{
				numero = Math.floor(Math.random() * 9) + 11; // 9 + 11 seleciona perguntas de indice 11 a 20 (nivel 2) e evita sorteio do 0
			}
			if (nivel ==3)
			{
				numero = Math.floor(Math.random() * 9) + 21; // 9 + 21 seleciona perguntas de indice 21 a 30 (nivel 3) e evita sorteio do 0
			}
			console.log("index: "+numero);
			geraPergunta();
			geraOpcao1();
			geraOpcao2();
			geraOpcao3();
			geraOpcao4();
			geraResposta();
			tirarVida = false;
			mostrarPergunta = false;
			reposiciona();
		}

		if(vida_inimigo == 60 && mostrarPergunta && countQuestao == 2)
		{
			$("#instrucao").remove();
			if (nivel ==1)
			{
				numero = Math.floor(Math.random() * 9) + 1; // 9 + 1 seleciona perguntas de indice 1 a 10 (nivel 1) e evita sorteio do 0
			}
			if (nivel ==2)
			{
				numero = Math.floor(Math.random() * 9) + 11; // 9 + 11 seleciona perguntas de indice 11 a 20 (nivel 2) e evita sorteio do 0
			}
			if (nivel ==3)
			{
				numero = Math.floor(Math.random() * 9) + 21; // 9 + 21 seleciona perguntas de indice 21 a 30 (nivel 3) e evita sorteio do 0
			}
			console.log("index: "+numero);
			geraPergunta();
			geraOpcao1();
			geraOpcao2();
			geraOpcao3();
			geraOpcao4();
			geraResposta();
			tirarVida = false;
			mostrarPergunta = false;
			reposiciona();
		}	

		if(vida_inimigo == 40 && mostrarPergunta && countQuestao == 3)
		{
			$("#instrucao").remove();
			if (nivel ==1)
			{
				numero = Math.floor(Math.random() * 9) + 1; // 9 + 1 seleciona perguntas de indice 1 a 10 (nivel 1) e evita sorteio do 0
			}
			if (nivel ==2)
			{
				numero = Math.floor(Math.random() * 9) + 11; // 9 + 11 seleciona perguntas de indice 11 a 20 (nivel 2) e evita sorteio do 0
			}
			if (nivel ==3)
			{
				numero = Math.floor(Math.random() * 9) + 21; // 9 + 21 seleciona perguntas de indice 21 a 30 (nivel 3) e evita sorteio do 0
			}
			console.log("index: "+numero);
			geraPergunta();
			geraOpcao1();
			geraOpcao2();
			geraOpcao3();
			geraOpcao4();
			geraResposta();
			tirarVida = false;
			mostrarPergunta = false;
			reposiciona();
		}

		if(vida_inimigo == 20 && mostrarPergunta && countQuestao == 4)
		{
			$("#instrucao").remove();
			if (nivel ==1)
			{
				numero = Math.floor(Math.random() * 9) + 1; // 9 + 1 seleciona perguntas de indice 1 a 10 (nivel 1) e evita sorteio do 0
			}
			if (nivel ==2)
			{
				numero = Math.floor(Math.random() * 9) + 11; // 9 + 11 seleciona perguntas de indice 11 a 20 (nivel 2) e evita sorteio do 0
			}
			if (nivel ==3)
			{
				numero = Math.floor(Math.random() * 9) + 21; // 9 + 21 seleciona perguntas de indice 21 a 30 (nivel 3) e evita sorteio do 0
			}
			console.log("index: "+numero);
			geraPergunta();
			geraOpcao1();
			geraOpcao2();
			geraOpcao3();
			geraOpcao4();
			geraResposta();
			tirarVida = false;
			mostrarPergunta = false;
			reposiciona();
		}

		if(vida_inimigo == 0)
		{
			venceu();
		}
		
	}
	// ########################  PLACAR ####################################

	function placar() 
	{	
		$("#placar").html("<h2> Pontos: " + pontos + " &nbsp; &nbsp; Questões respondidas: " 
		+ respondidas + "&nbsp; &nbsp; Certas:"+ certas
		+ "&nbsp; &nbsp; Erradas:"+ erradas +  "</h2>");
	} //fim da função placar()

	// ########################  Vidas ####################################
	function vida() 
	{
		if (vidaAtual==3) 
		{
			$("#vida").css("background-image", "url(imgs/energia3.png)");
		}
	
		if (vidaAtual==2) 
		{
			$("#vida").css("background-image", "url(imgs/energia2.png)");
		}
	
		if (vidaAtual==1) 
		{
			$("#vida").css("background-image", "url(imgs/energia1.png)");
		}
	
		if (vidaAtual==0) 
		{
			gameOver();
		}
	} // Fim da função vida()

	//########################  vida inimigo ####################################

	function vidainimigo() 
	{	
		$("#vidainimigo").html("<h2> Vida do inimigo:"+vida_inimigo+"%" +"</h2>");
	} //fim da função placar()

	//########################  FUNÇÃO QUE FAZ O INIMIGO ATIRAR ####################################
	function inimigoatira()
	{
	 	
		somDisparo.play();
		topo_inimigo = parseInt($("#inimigo").css("top"));
		posicaoX_inimigo = parseInt($("#inimigo").css("left"));
		$("#tabuleiro").append("<div id='disparoinimigo' class='inimigoatira'></div>");
		$("#disparoinimigo").css("top",topo_inimigo);
		$("#disparoinimigo").css("left",posicaoX_inimigo);

		var Disparo = window.setInterval(Disp_inimigo,30);
		
	
		function Disp_inimigo () 
		{
		 	topo = parseInt($("#disparoinimigo").css("top"));
		 	$("#disparoinimigo").css("top",topo-30);

		 		if(topo <= 0)
		 		{
		 			window.clearInterval(Disparo);
		 			Disparo = null;
					 $("#disparoinimigo").remove();
					 inimigoatira_parte2();
					
				 } 
				 
				 function inimigoatira_parte2()
					{
						
						somDisparo.play();
						posicaoX_jogador = parseInt($("#jogador").css("left"));
						$("#tabuleiro").append("<div id='disparoinimigo2' class='inimigoatira2'></div>");
						$("#disparoinimigo2").css("top",0);
						$("#disparoinimigo2").css("left",posicaoX_jogador);

						var Disparo2 = window.setInterval(Disp_inimigo2,30);
					
						function Disp_inimigo2 () 
						{
							topo2 = parseInt($("#disparoinimigo2").css("top"));
							$("#disparoinimigo2").css("top",topo2+30);
							var colisao_disparoinimigo = ($("#disparoinimigo2").collision($("#jogador")));

								if (colisao_disparoinimigo.length>0) 
								{
									window.clearInterval(Disparo2);
									Disparo2 = null;
									$("#disparoinimigo2").remove();
									explosao();
									vidaAtual = vidaAtual - 1;
								} 
						}//fim funcao Disp_inimigo2()
		 }//fim funcao Disp_inimigo()
	}

	}

// jogador explode
	function explosao() {
		jogadorX = parseInt($("#jogador").css("left"));
		jogadorY = parseInt($("#jogador").css("top"));
		somExplosao.play();
		$("#tabuleiro").append("<div id='explosao'></div");
		$("#explosao").css("background-image", "url(imgs/explosao.png)");
		var div=$("#explosao");
		div.css("top", jogadorY);
		div.css("left", jogadorX);
		div.animate({width:200, opacity:0}, "slow");
	
		var tempoExplosao=window.setInterval(removeExplosao, 1000);
	
		function removeExplosao() {
			div.remove();
			window.clearInterval(tempoExplosao);
			tempoExplosao=null;
			
		}
	}

// inimigo explode
	function explosaoinimigo() {
		inimigoX = parseInt($("#inimigo").css("left"));
		inimigoY = parseInt($("#inimigo").css("top"));
		somExplosao.play();
		$("#tabuleiro").append("<div id='explosao'></div");
		$("#explosao").css("background-image", "url(imgs/explosao.png)");
		var div=$("#explosao");
		div.css("top", inimigoY);
		div.css("left", inimigoX);
		div.animate({width:200, opacity:0}, "slow");
	
		var tempoExplosao=window.setInterval(removeExplosao, 1000);
	
		function removeExplosao() {
			div.remove();
			window.clearInterval(tempoExplosao);
			tempoExplosao=null;
			
		}
	}
//Reposiciona jogador no centro quando a pergunta aparece
	function reposiciona()
	{
		$("#jogador").css("left",190);
		$("#jogador").css("top",300);
	}
// Exibe as intruções do jogo no topo da tela
	function instrucao()
	{
		if (exibirInstrucao)
		{
			$("#fundoGame").append("<div id='instrucao'>Mova-se e atire contra o inimigo </div>");
			exibirInstrucao = false;
		}
	}
// Função Game Over
	function gameOver() {
		fimdejogo = true;
		musica.pause();
		somGameover.play();
		exibirInstrucao = false;
		window.clearInterval(jogo.timer);
		jogo.timer=null;
		$("#vida").remove();
		$("#placar").remove();
		$("#vidainimigo").remove();
		$("#jogador").remove();
		$("#inimigo").remove();
		$("#tabuleiro").remove();
		$("#fundoGame").append("<div id='fim'> </div>");
		$("#fim").html("<h1> Game Over </h1><p>Sua pontuação foi: " + pontos + "<br>Perguntas respontidas: " + respondidas +
		"<br>Certas: " + certas + "<br> Erradas: " + erradas+ "</p>"
		+ "<div id='reinicia' onClick=reiniciaJogo()><h3>Jogar Novamente</h3></div>");
	}
// Função de vitória
	function venceu() {
		fimdejogo = true;
		musica.pause();
		somGameover.play();
		exibirInstrucao = false;
		window.clearInterval(jogo.timer);
		jogo.timer=null;
		nivel = nivel+1;
		$("#vida").remove();
		$("#placar").remove();
		$("#vidainimigo").remove();
		$("#jogador").remove();
		$("#inimigo").remove();
		$("#tabuleiro").remove();
		$("#instrucao").remove();
		$("#fundoGame").append("<div id='fim'> </div>");
		if (nivel <= 3)
		{
		$("#fim").html("<h1> PARABENS!! </h1><p>Sua pontuação foi: " + pontos + "<br>Perguntas respontidas: " + respondidas +
		"<br>Certas: " + certas + "<br> Erradas: " + erradas+ "</p>"
		+ "<div id='reinicia' onClick=reiniciaJogo()><h3>Jogar Novamente</h3></div>"
		+ "<div id='avanca' onClick=avancarJogo()><h3>Ir para Nível "+nivel+"</h3></div>");
		} 
		else
		{
		$("#fim").html("<h1> PARABENS!! </h1><p>Sua pontuação foi: " + pontos + "<br>Perguntas respontidas: " + respondidas +
		"<br>Certas: " + certas + "<br> Erradas: " + erradas+ "</p>"
		+ "<div id='reinicia' onClick=reiniciaJogo()><h3>Jogar Novamente</h3></div>");
		} 
		
	}
	
} // Fim da função start 
